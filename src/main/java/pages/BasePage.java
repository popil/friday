package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

abstract public class BasePage {
    private static final Integer TIMEOUT = 10;

    public WebDriver driver;
    private WebDriverWait wait;
    protected String expectedUrl;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, TIMEOUT);
    }

    public void validateUrl() {
        wait.until(ExpectedConditions.urlContains(expectedUrl));
    }
}
