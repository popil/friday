package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;
import pages.elements.BaseEditBox;

import static tools.WebDriverManager.getDriver;


public class SelectModelPage extends CalculateInsuranceFlowPage {
    public static class ModelEditBox extends BaseEditBox {
        ModelEditBox() {
            super(new By.ByCssSelector("input[name='modelFilter']"));
        }
    }

    public static class ModelOptionButton extends BaseButton<SelectBodyTypePage> {
        ModelOptionButton() {
            super(new By.ByCssSelector("button[class^='SingleClickListField']"));
        }

        @Override
        public SelectBodyTypePage goTo() {
            return new SelectBodyTypePage(getDriver());
        }
    }

    public SelectModelPage(WebDriver driver) {
        super(driver);
        this.expectedUrl = "/selectModel";
    }

    public ModelEditBox modelEditBox = new ModelEditBox();
    public ModelOptionButton modelOptionButton = new ModelOptionButton();
}
