package pages;

import org.openqa.selenium.WebDriver;


public class EnterBirthDatePage extends CalculateInsuranceFlowPage {
    public EnterBirthDatePage(WebDriver driver) {
        super(driver);
        this.expectedUrl = "/enterBirthDate";
    }
}
