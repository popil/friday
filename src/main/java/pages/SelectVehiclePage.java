package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;
import pages.elements.BaseEditBox;

import static tools.WebDriverManager.getDriver;


public class SelectVehiclePage extends CalculateInsuranceFlowPage {
    public static class MakeEditBox extends BaseEditBox {
        MakeEditBox() {
            super(new By.ByCssSelector("input[name='makeFilter']"));
        }
    }

    public static class MakeOptionButton extends BaseButton<SelectModelPage> {
        MakeOptionButton() {
            super(new By.ByCssSelector("button[class^='SingleClickListField']"));
        }

        @Override
        public SelectModelPage goTo() {
            return new SelectModelPage(getDriver());
        }
    }

    public SelectVehiclePage(WebDriver driver) {
        super(driver);
        this.expectedUrl = "/selectVehicle";
    }

    public MakeEditBox makeEditBox = new MakeEditBox();
    public MakeOptionButton makeOptionButton = new MakeOptionButton();
}


