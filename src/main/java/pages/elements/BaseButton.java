package pages.elements;

import org.openqa.selenium.By;

public abstract class BaseButton<PAGE> extends BaseElement<PAGE> {
    public BaseButton(By locator) {
        super(locator);
    }

    public PAGE click() {
        waitForClickableElement().click();
        return goTo();
    }

    public String getText() {
        return waitForClickableElement().getText();
    }
}
