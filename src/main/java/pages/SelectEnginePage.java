package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;

import static tools.WebDriverManager.getDriver;


public class SelectEnginePage extends CalculateInsuranceFlowPage {
    public static class EngineButton extends BaseButton<EnterRegistrationDatePage> {
        EngineButton() {
            super(new By.ByCssSelector("button[name='engine']"));
        }

        @Override
        public EnterRegistrationDatePage goTo() {
            return new EnterRegistrationDatePage(getDriver());
        }
    }

    public SelectEnginePage(WebDriver driver) {
        super(driver);
        this.expectedUrl = "/selectEngine";
    }

    public EngineButton engineButton = new EngineButton();
}
