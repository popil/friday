package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;


public class CalculateInsuranceFlowPage extends BasePage {
    public static class BackButton extends BaseButton {
        BackButton() {
            super(new By.ByCssSelector("button[data-test-id='wizardBackButton']"));
        }
    }

    public static class ContinueButton<PAGE> extends BaseButton<PAGE> {
        public PAGE goToPage;

        public ContinueButton(PAGE goToPage) {
            super(new By.ByCssSelector("button[type='submit']"));
            this.goToPage = goToPage;
        }

        @Override
        public PAGE goTo() {
            return goToPage;
        }

        public Boolean isDisabled() {
            return super.findElement().getAttribute("class").contains("disabled");
        }
    }

    public CalculateInsuranceFlowPage(WebDriver driver) {
        super(driver);
    }
}
