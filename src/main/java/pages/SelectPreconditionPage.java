package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;
import pages.elements.BaseEditBox;


public class SelectPreconditionPage extends CalculateInsuranceFlowPage {
    public static class KeepingCarButton extends BaseButton {
        KeepingCarButton() {
            super(new By.ByCssSelector("button[value='keepingCar']"));
        }
    }


    public static class BuyingCarButton extends BaseButton {
        BuyingCarButton() {
            super(new By.ByCssSelector("button[value='buyingCar']"));
        }
    }


    public static class StartDateEditBox extends BaseEditBox {
        StartDateEditBox() {
            super(new By.ByName("inceptionDate"));
        }
    }


    public SelectPreconditionPage(WebDriver driver) {
        super(driver);
        this.expectedUrl = "/selectPrecondition";
    }

    public KeepingCarButton keepingCarBtn = new KeepingCarButton();
    public BuyingCarButton buyingCarButton = new BuyingCarButton();
    public StartDateEditBox startDateEditBox = new StartDateEditBox();
    public ContinueButton<SelectOwnerPage> continueButton = new ContinueButton<>(new SelectOwnerPage(driver));

}

