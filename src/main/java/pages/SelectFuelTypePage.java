package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;

import static tools.WebDriverManager.getDriver;


public class SelectFuelTypePage extends CalculateInsuranceFlowPage {
    public static class FuelTypeButton extends BaseButton<SelectEnginePowerPage> {
        FuelTypeButton(String name) {
            super(new By.ByXPath("//button[@name='fuelType'][.='" + name + "']"));
        }

        @Override
        public SelectEnginePowerPage goTo() {
            return new SelectEnginePowerPage(getDriver());
        }
    }

    public SelectFuelTypePage(WebDriver driver) {
        super(driver);
        this.expectedUrl = "/selectFuelType";
    }

    public FuelTypeButton getFuelTypeButton(String fuelType) {
        return new FuelTypeButton(fuelType);
    }
}
