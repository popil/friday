package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseEditBox;


public class EnterRegistrationDatePage extends CalculateInsuranceFlowPage {
    public static class RegistrationDateEditBox extends BaseEditBox {
        RegistrationDateEditBox() {
            super(new By.ByCssSelector("input[name='monthYearFirstRegistered']"));
        }
    }

    public EnterRegistrationDatePage(WebDriver driver) {
        super(driver);
        this.expectedUrl = "/enterRegistrationDate";
    }

    public RegistrationDateEditBox enginePowerEditBox = new RegistrationDateEditBox();
    public ContinueButton<EnterBirthDatePage> continueButton = new ContinueButton<>(new EnterBirthDatePage(driver));
}
