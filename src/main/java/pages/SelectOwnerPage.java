package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;


public class SelectOwnerPage extends CalculateInsuranceFlowPage {
    public static class OwnerButton extends BaseButton {
        OwnerButton() {
            super(new By.ByCssSelector("[data-test-id='shared.yes']"));
        }
    }

    public static class NonOwnerButton extends BaseButton {
        NonOwnerButton() {
            super(new By.ByCssSelector("[data-test-id='shared.no']"));
        }
    }


    public static class UsedCarButton extends BaseButton {
        UsedCarButton() {
            super(new By.ByCssSelector("button[value='used']"));
        }
    }


    public static class NewCarButton extends BaseButton {
        NewCarButton() {
            super(new By.ByCssSelector("button[value='brandNew']"));
        }
    }


    public SelectOwnerPage(WebDriver driver) {
        super(driver);
        this.expectedUrl = "/selectRegisteredOwner";
    }

    public OwnerButton ownerButton = new OwnerButton();
    public NonOwnerButton nonOwnerButton = new NonOwnerButton();
    public UsedCarButton usedCarButton = new UsedCarButton();
    public NewCarButton newCarButton = new NewCarButton();
    public ContinueButton<SelectVehiclePage> continueButton = new ContinueButton<>(new SelectVehiclePage(driver));
}
