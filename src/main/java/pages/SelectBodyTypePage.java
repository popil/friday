package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;

import static tools.WebDriverManager.getDriver;


public class SelectBodyTypePage extends CalculateInsuranceFlowPage {
    public static class BodyTypeButton extends BaseButton<SelectFuelTypePage> {
        BodyTypeButton(String name) {
            super(new By.ByXPath("//button[@name='bodyType'][.='" + name + "']"));
        }

        @Override
        public SelectFuelTypePage goTo() {
            return new SelectFuelTypePage(getDriver());
        }
    }

    public SelectBodyTypePage(WebDriver driver) {
        super(driver);
        this.expectedUrl = "/selectBodyType";
    }


    public BodyTypeButton getBodyTypeButton(String name) {
        return new BodyTypeButton(name);
    }
}
