package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;

import static tools.WebDriverManager.getDriver;


public class SelectEnginePowerPage extends CalculateInsuranceFlowPage {
    public static class EnginePowerOptionButton extends BaseButton<SelectEnginePage> {
        EnginePowerOptionButton(String name) {
            super(new By.ByXPath("//button[@name='enginePower']['" + name + "']"));
        }

        @Override
        public SelectEnginePage goTo() {
            return new SelectEnginePage(getDriver());
        }
    }

    public SelectEnginePowerPage(WebDriver driver) {
        super(driver);
        this.expectedUrl = "/selectEnginePower";
    }

    public EnginePowerOptionButton getEnginePowerButton(String name) {
        return new EnginePowerOptionButton(name);
    }
}
