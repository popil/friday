package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;

import static tools.WebDriverManager.getDriver;


public class HomePage extends BasePage {
    public static class CalculateInsuranceButton extends BaseButton<SelectPreconditionPage> {
        CalculateInsuranceButton() {
            super(new By.ById("erleben-button"));
        }

        @Override
        public SelectPreconditionPage goTo() {
            return new SelectPreconditionPage(getDriver());
        }
    }

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public CalculateInsuranceButton calculateInsuranceBtn = new CalculateInsuranceButton();
}


