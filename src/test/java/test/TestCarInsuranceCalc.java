package test;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.*;

import java.time.format.DateTimeFormatter;
import java.time.LocalDate;

public class TestCarInsuranceCalc extends BaseTest {
    private String getDateFromToday(Integer days, String pattern) {
        LocalDate todayDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return todayDate.plusDays(days).format(formatter);
    }

    @Test
    public void testStartDateValidation() {
        final String YESTERDAY = getDateFromToday(-1, "dd.MM.yyyy");
        final String TOMORROW = getDateFromToday(1, "dd.MM.yyyy");

        HomePage homepage = new HomePage(driver);
        SelectPreconditionPage selectPreconditionPage = homepage.calculateInsuranceBtn.click();
        selectPreconditionPage.keepingCarBtn.click();

        selectPreconditionPage.startDateEditBox.sendText(YESTERDAY);
        Assert.assertTrue(selectPreconditionPage.continueButton.isDisabled(), "Past start date accepted!");

        selectPreconditionPage.startDateEditBox.clear();
        Assert.assertTrue(selectPreconditionPage.continueButton.isDisabled(), "Empty start date accepted!");

        selectPreconditionPage.startDateEditBox.sendText(TOMORROW);
        selectPreconditionPage.startDateEditBox.doubleClick();
        selectPreconditionPage.startDateEditBox.pressBackspace();
        Assert.assertTrue(selectPreconditionPage.continueButton.isDisabled(), "Empty start date accepted!");
    }

    @DataProvider(name = "provideCarModel")
    public Object[][] provideData() {
        return new Object[][] {
                { "Audi", "A3", "Cabrio", "Diesel", "103 kW / 140 PS" },
                { "Audi", "Q5", "", "Benzin", "260 kW / 354 PS" },
                { "Toyota", "COROLLA", "Limousine", "Benzin", "77 kW / 105 PS" },
                { "Toyota", "AURIS", "Limousine", "Diesel", "66 kW / 90 PS" },
                { "Volvo", "240", "Kombi", "Benzin", "81 kW / 110 PS" },
                { "Volvo", "XC60", "", "Hybrid", "235 kW / 320 PS" },
        };
    }

    @Test(dataProvider = "provideCarModel")
    public void testSelectCarFlow(String make, String model, String body, String fuel, String power) {
        final String YEAR_AGO = getDateFromToday(-365, "MM.yyyy");

        HomePage homepage = new HomePage(driver);
        SelectPreconditionPage selectPreconditionPage = homepage.calculateInsuranceBtn.click();
        selectPreconditionPage.validateUrl();
        selectPreconditionPage.keepingCarBtn.click();
        SelectOwnerPage selectOwnerPage = selectPreconditionPage.continueButton.click();

        selectOwnerPage.validateUrl();
        selectOwnerPage.ownerButton.click();
        selectOwnerPage.newCarButton.click();
        SelectVehiclePage selectVehiclePage = selectOwnerPage.continueButton.click();

        selectVehiclePage.validateUrl();
        selectVehiclePage.makeEditBox.sendText(make);
        SelectModelPage selectModelPage = selectVehiclePage.makeOptionButton.click();

        selectModelPage.validateUrl();
        selectModelPage.modelEditBox.sendText(model);
        SelectBodyTypePage selectBodyTypePage = selectModelPage.modelOptionButton.click();

        SelectFuelTypePage selectFuelTypePage = new SelectFuelTypePage(driver);
        if (!body.isEmpty()) {
            selectBodyTypePage.validateUrl();
            selectFuelTypePage = selectBodyTypePage.getBodyTypeButton(body).click();
            selectFuelTypePage.validateUrl();
        }

        SelectEnginePowerPage selectEnginePowerPage = selectFuelTypePage.getFuelTypeButton(fuel).click();

        selectEnginePowerPage.validateUrl();
        SelectEnginePage selectEnginePage = selectEnginePowerPage.getEnginePowerButton(power).click();

        selectEnginePage.validateUrl();
        String engineData = selectEnginePage.engineButton.getText();
        Assert.assertTrue(engineData.contains(model), "Car model name missing in engine data");

        EnterRegistrationDatePage enterRegistrationDatePage = selectEnginePage.engineButton.click();
        enterRegistrationDatePage.enginePowerEditBox.sendText(YEAR_AGO);

        EnterBirthDatePage enterBirthDatePage = enterRegistrationDatePage.continueButton.click();
    }
}
