#### Prerequisites
  - Java 8
  - Maven
  - TestNG
  - Selenium
  - Chromedriver should be in system PATH
  
#### Getting started
  - clone the project
  - in the terminal open ```friday``` project root directory
  - run ```mvn install```
  - run tests ```mvn test```
  
#### Bug?
  User can proceed with empty start date when "car is already insured" option selected
  
###### STEPS TO REPRODUCE
  1. Go to https://hello.friday.de
  2. Select "Das Auto ist schon versichert"
  3. Focus on "Wann soll deine FRIDAY Versicherung starten?" field
  4. Press CTRL+A (or mouse double click), press BACKSPACE
  5. Click "Weiter" button
###### ACTUAL RESULT
  User proceeds to the Select Registered Owner page
###### EXPECTED RESULT
  + Warning message should appear below "Wann soll deine FRIDAY Versicherung starten?" field
  + "Weiter" button should be disabled
  