QA Automation self didactic exercise
------

Dear candidate,   
We appreciate your interest at FRIDAY for QA automation position. We would like to test your self learning and software skills.   
We would ask you to implement a UI test that goes through our [sales funnel]. Your test should stop after picking a car**.   
We ask that your test will pick at least three different car brands, each brand with two different cars.
From a QA perspective: Can you think of further scenarios (e.g. negative ones)? Can you implement at least one?

   
##### Technologies to pick from    
| Technology    | Stack                         |
|---------------| :-----------------------------|
| Build tool    | [Maven] / [Gradle] / [yarn]          |   
| Language      | [Java] / [Groovy]/ [Typescript] (js)  |   
| UI technology | [Selenium] / [Geb] / [WebdriverIO]   |
| Test framework| [Junit 5]/ [TestNG] / [Spock]            |


###### Bonus points:   
- CI integration   
- Taking screen shots   
- [Jacoco] html report integration   
- Documentation of code and [project][readme]    

\** Stop when you have to enter your birth date (_Wann wurdest du geboren?_)
### Join US!
![alt text](https://friday-landing.cdn.prismic.io/friday-landing/16e0bdd98142429776ad3c29a049fe44043185ed_ppkm-card-mobile.jpg)

[sales funnel]: https://hello.friday.de/

[maven]: https://maven.apache.org/
[gradle]: https://gradle.org/
[yarn]: https://yarnpkg.com/en/

[java]: https://docs.oracle.com/javase/8/docs/technotes/guides/language/index.html
[groovy]: https://groovy-lang.org/
[typescript]: https://www.typescriptlang.org/

[selenium]: https://www.seleniumhq.org/
[Geb]: https://gebish.org/
[webdriverio]: https://webdriver.io/

[junit 5]: https://junit.org/junit5/
[testng]: https://testng.org/doc/
[spock]: http://spockframework.org/

[jacoco]: https://www.jacoco.org/jacoco/trunk/index.html
[readme]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
